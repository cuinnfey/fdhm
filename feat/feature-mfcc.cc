// feat/feature-mfcc.cc

// Copyright 2009-2011  Karel Vesely;  Petr Motlicek
//                2016  Johns Hopkins University (author: Daniel Povey)

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.


#include "feat/feature-mfcc.h"
#include <fstream>

namespace kaldi {

//V/ HISTOGRAM MATCHING ADDITIONS /V//

vector_clump histMatch(
               VectorBase<BaseFloat> *src,
               VectorBase<BaseFloat> *tmp,
               VectorBase<BaseFloat> *mtc,
               BaseFloat silence_threshold) {


  //KALDI_VLOG(2) << "IN";
  /// Catch output of unique_info. Consider doing 'auto'               
  MatrixBase<BaseFloat> *cdf_mat_src = unique_info(src);
  MatrixBase<BaseFloat> *cdf_mat_tmp = unique_info(tmp);

  //KALDI_VLOG(2) << "cdf_mat_src->NumRows() = " << cdf_mat_src->NumRows();
  //KALDI_VLOG(2) << "cdf_mat_src->NumCols() = " << cdf_mat_src->NumCols();

  /// Initialize cdf vecs  
  VectorBase<BaseFloat> *s_quantiles = new Vector<BaseFloat>(cdf_mat_src->NumCols(), 
                                                             kUndefined);
  s_quantiles->SetZero();
  VectorBase<BaseFloat> *t_quantiles = new Vector<BaseFloat>(cdf_mat_tmp->NumCols(), 
                                                             kUndefined);
  t_quantiles->SetZero();
  VectorBase<BaseFloat> *m_quantiles = new Vector<BaseFloat>(cdf_mat_src->NumCols(), 
                                                             kUndefined);
  m_quantiles->SetZero();

  

  /// Pulling vectors out of cdf matrices
  /// Values
  VectorBase<BaseFloat> *vals_src = new Vector<BaseFloat>(cdf_mat_src->NumCols(),
                                                      kUndefined);
  vals_src->CopyRowFromMat(*cdf_mat_src, 0);

  VectorBase<BaseFloat> *vals_tmp = new Vector<BaseFloat>(cdf_mat_tmp->NumCols(),
                                                      kUndefined);
  vals_tmp->CopyRowFromMat(*cdf_mat_tmp, 0);

  /// Indices
  VectorBase<BaseFloat> *idxs_src = new Vector<BaseFloat>(cdf_mat_src->NumCols(),
                                                      kUndefined);
  idxs_src->CopyRowFromMat(*cdf_mat_src, 1);
  VectorBase<BaseFloat> *idxs_tmp = new Vector<BaseFloat>(cdf_mat_tmp->NumCols(),
                                                      kUndefined);
  idxs_tmp->CopyRowFromMat(*cdf_mat_tmp, 1);
 
  /// Counts
  VectorBase<BaseFloat> *cnts_src = new Vector<BaseFloat>(cdf_mat_src->NumCols(),
                                                      kUndefined);
  cnts_src->CopyRowFromMat(*cdf_mat_src, 2);
  VectorBase<BaseFloat> *cnts_tmp = new Vector<BaseFloat>(cdf_mat_tmp->NumCols(),
                                                      kUndefined);
  cnts_tmp->CopyRowFromMat(*cdf_mat_tmp, 2);
  //KALDI_VLOG(2) << "HERE";
  cumsum(cnts_src, s_quantiles);
  cumsum(cnts_tmp, t_quantiles);

  /// Normalize cdfs
  float max_src = 1.0000000f / (float) s_quantiles->Max();
  s_quantiles->Scale(max_src);

  float max_tmp = 1.0000000f / (float) t_quantiles->Max();
  t_quantiles->Scale(max_tmp);
         
  /// Initialize interp vector
  VectorBase<BaseFloat> *interp_vals = new Vector<BaseFloat>(vals_src->Dim(), kUndefined);
  interp_vals->SetZero();
  
  //KALDI_VLOG(2) << "HERE";
  
  /// interp between source and template cdfs
  linInterp(s_quantiles, vals_src,  t_quantiles, vals_tmp, interp_vals, silence_threshold); 

  /// For each index in idxs_src write the value of interp_val
  /// at that index to each index in the output vector
  // int idx_mtc = 0;
  for (int i = 0; i < interp_vals->Dim(); i++) {
    int index_source = *(idxs_src->Data() + i);
    //*(mtc->Data() + i) = *(interp_vals->Data() + index_source);
    //idx_mtc++;
    *(mtc->Data() + index_source) = *(interp_vals->Data() + i);
  }

  /// Catch match matrix that contains values, indexs, and counts
  MatrixBase<BaseFloat> *cdf_mat_mtc = unique_info(mtc);
  VectorBase<BaseFloat> *vals_mtc = new Vector<BaseFloat>(cdf_mat_mtc->NumCols(),
                                                      kUndefined);
  vals_mtc->CopyRowFromMat(*cdf_mat_mtc, 0);
  VectorBase<BaseFloat> *idxs_mtc = new Vector<BaseFloat>(cdf_mat_mtc->NumCols(),
                                                      kUndefined);
  idxs_mtc->CopyRowFromMat(*cdf_mat_mtc, 1);
  VectorBase<BaseFloat> *cnts_mtc = new Vector<BaseFloat>(cdf_mat_mtc->NumCols(),
                                                      kUndefined);
  cnts_mtc->CopyRowFromMat(*cdf_mat_mtc, 2);

  /// Take the cumulative sum of the counts and store it in the quantiles
  cumsum(cnts_mtc, m_quantiles);
 
  /// Prepare data for return
  VectorBase<BaseFloat> *cdf_src = s_quantiles;
  VectorBase<BaseFloat> *cdf_tmp  = t_quantiles; 
  VectorBase<BaseFloat> *cdf_mtc = m_quantiles;
  struct vector_clump cdfs = {cdf_src, vals_src, 
                              cdf_tmp, vals_tmp, 
                              cdf_mtc, vals_mtc};

 return cdfs;
           
}

MatrixBase<BaseFloat>* unique_info(VectorBase<BaseFloat> *ar) {
  
  /// Duplicate *ar
  VectorBase<BaseFloat> *ar_dup = new Vector<BaseFloat>(ar->Dim(),
                                             kUndefined);
  ar_dup->CopyFromVec(*ar);
  
  //KALDI_VLOG(2) << "ar_dup before sort" << *ar_dup;
  /// Sort ar_dup from least to greatest
  std::sort(ar_dup->Data(), ar_dup->Data() + (ar_dup->Dim()));
  
  //KALDI_VLOG(2) << "ar_dup after sort" << *ar_dup;
  /// Remove duplicate values
  std::unique(ar_dup->Data(), ar_dup->Data() + (ar_dup->Dim()));

  //KALDI_VLOG(2) << "ar_dup after unique" << *ar_dup;
  /// Initialize matrix to store values, idices, and counts
  MatrixBase<BaseFloat> *cdfs = new Matrix<BaseFloat>(3, ar_dup->Dim());
  cdfs->SetZero();


  /// Store values, calculate indecies and counts
  cdfs->CopyRowFromVec(*ar_dup, 0);  //might need to resize ar_dup somone before copy
  float Eps = 0.00000001f;
  for (int i = 0; i < cdfs->NumCols(); i++) {
    //int idx = 0;
    for (int j = 0; j < ar->Dim(); j++) {
      float diff = *(cdfs->RowData(0) + i) - *(ar->Data() + j);
      if (diff < Eps && diff > -Eps) {
        *(cdfs->RowData(1) + i) = j;
        *(cdfs->RowData(2) + i) += 1;
      }
    }
  }

  return cdfs;

}

void cumsum(VectorBase<BaseFloat> *cnts,
            VectorBase<BaseFloat> *quantiles) {
  
  for (int i = 0; i < cnts->Dim(); i++) {
    if (0 == i) {
      *(quantiles->Data() + i) = *(cnts->Data() + i);
    } else {
      *(quantiles->Data() + i) = *(quantiles->Data() + (i - 1))
                           + *(cnts->Data() + (i - 1));
    }
  }
  
  return;
}

float interpolated(float target, float x1, float x2, float y1, float y2) {
  float y = y2 - y1;
  float x = x2 - x1;
  return (((target - y1) * x) / y) + x1;
}

 
void linInterp(VectorBase<BaseFloat> *s_quan,
               VectorBase<BaseFloat> *s_vals,
               VectorBase<BaseFloat> *t_quan,
               VectorBase<BaseFloat> *t_vals,
               VectorBase<BaseFloat> *t_interp,
               BaseFloat silence_threshold) {
  /*
  for (int n = 0; n < t_vals->Dim(); n++) {
    int j = 0;
    for (int i = 0; i < s_quan->Dim(); i++) {
      if ((*(t_vals->Data() + n) >= *(s_quan->Data() + i)) 
          && (*(t_vals->Data() + n) <= *(s_quan->Data() + (i + 1)))) {
        j=i;
        break;
      }
    }
    float nv = interpolated(*(t_vals->Data() + n), *(s_quan->Data() + j), 
                            *(s_quan->Data() + (j+1)), *(t_quan->Data() + j),
                                                       *(t_quan->Data() + (j+1)));
    *(t_interp->Data() + n) = nv;
  }
  return;
  */
 
  /// for each value in source quantiles and for each value in 
  /// template quantiles, find the values of template quantiles that
  /// the value of source quantiles is in between, then linear
  /// interpolate between those points to give a new bin value
  /// to the corresponding source_quantile value
  for(int j = 0; j < s_quan->Dim(); j++) {
    for (int i = 1; i < t_quan->Dim(); i++) {
      if (*(t_quan->Data() + (i - 1)) <= *(s_quan->Data() + j) && *(s_quan->Data() + j) <= *(t_quan->Data() + i)) {
        if (*(s_vals->Data() + j) >= silence_threshold) {
          *(t_interp->Data() + j) = interpolated(*(s_quan->Data() + j),
                                               *(t_vals->Data() + (i - 1)),
                                               *(t_vals->Data() + i),
                                               *(t_quan->Data() + (i - 1)),
                                               *(t_quan->Data() + i));
         } else {
          *(t_interp->Data()+ j) = *(s_vals->Data() + j);
         }
         break;      
      }
    }
  }

  return;

}

//^/ HISTOGRAM MATCHING ADDITIONS /^//


void MfccComputer::Compute(BaseFloat signal_raw_log_energy,
                           BaseFloat vtln_warp,
                           VectorBase<BaseFloat> *signal_frame,
                           VectorBase<BaseFloat> *feature) {
  KALDI_ASSERT(signal_frame->Dim() == opts_.frame_opts.PaddedWindowSize() &&
               feature->Dim() == this->Dim());

  const MelBanks &mel_banks = *(GetMelBanks(vtln_warp));

  if (opts_.use_energy && !opts_.raw_energy)
    signal_raw_log_energy = Log(std::max<BaseFloat>(VecVec(*signal_frame, *signal_frame),
                                     std::numeric_limits<float>::epsilon()));

  if (srfft_ != NULL)  // Compute FFT using the split-radix algorithm.
    srfft_->Compute(signal_frame->Data(), true);
  else  // An alternative algorithm that works for non-powers-of-two.
    RealFft(signal_frame, true);

  // Convert the FFT into a power spectrum.
  ComputePowerSpectrum(signal_frame);
  SubVector<BaseFloat> power_spectrum(*signal_frame, 0,
                                      signal_frame->Dim() / 2 + 1);

  mel_banks.Compute(power_spectrum, &mel_energies_);

  // avoid log of zero (which should be prevented anyway by dithering).
  mel_energies_.ApplyFloor(std::numeric_limits<float>::epsilon());
  mel_energies_.ApplyLog();  // take the log.

  feature->SetZero();  // in case there were NaNs.
  // feature = dct_matrix_ * mel_energies [which now have log]
  //V/ HISTOGRAM MATCHING REMOVALS /V//
  //feature->AddMatVec(1.0, dct_matrix_, kNoTrans, mel_energies_, 0.0);
  //^/ HISTOGRAM MATCHING REMOVALS /^//
  

  //V/ HISTOGRAM MATCHING ADDITIONS /V//
  
  feature->CopyFromVec(mel_energies_);
  //^/ HISTOGRAM MATCHING ADDITIONS /^//
  
  //V/ HISTOGRAM MATCHING REMOVALS /V//
  /* 
  if (opts_.cepstral_lifter != 0.0)
    feature->MulElements(lifter_coeffs_);

  if (opts_.use_energy) {
    if (opts_.energy_floor > 0.0 && signal_raw_log_energy < log_energy_floor_)
      signal_raw_log_energy = log_energy_floor_;
    (*feature)(0) = signal_raw_log_energy;
  }

  if (opts_.htk_compat) {
    BaseFloat energy = (*feature)(0);
    for (int32 i = 0; i < opts_.num_ceps - 1; i++)
      (*feature)(i) = (*feature)(i+1);
    if (!opts_.use_energy)
      energy *= M_SQRT2;  // scale on C0 (actually removing a scale
    // we previously added that's part of one common definition of
    // the cosine transform.)
    (*feature)(opts_.num_ceps - 1)  = energy;
  }
  */ 
  //^/ HISTOGRAM MATCHING REMOVALS /^//
}

//V/HISTOGRAM MATCHING ADDITIONS/V//



void MfccComputer::ComputeDCT(BaseFloat signal_raw_log_energy,
                           BaseFloat vtln_warp,
                           VectorBase<BaseFloat> *signal_frame,
                           VectorBase<BaseFloat> *feature,
                           VectorBase<BaseFloat> *feature_temp) {
   
  feature_temp->AddMatVecSame(1.0, dct_matrix_, kNoTrans, *feature, 0.0);
  
  if (opts_.cepstral_lifter != 0.0)
    feature_temp->MulElements(lifter_coeffs_);
  if (opts_.use_energy) {
    if (opts_.energy_floor > 0.0 && signal_raw_log_energy < log_energy_floor_)
      signal_raw_log_energy = log_energy_floor_;
    (*feature_temp)(0) = signal_raw_log_energy;
  }
  
  if (opts_.htk_compat) {
    BaseFloat energy = (*feature_temp)(0);
    for (int32 i = 0; i < opts_.num_ceps - 1; i++)
      (*feature_temp)(i) = (*feature_temp)(i+1);
      if (!opts_.use_energy)
        energy *= M_SQRT2;  // scale on C0 (actually removing a scale
      // we previously added that's part of one common definition of
      // the cosine transform.)
      (*feature_temp)(opts_.num_ceps - 1)  = energy;
  }
}

//^/HISTOGRAM MATCHING ADDITIONS/^//


MfccComputer::MfccComputer(const MfccOptions &opts):
    opts_(opts), srfft_(NULL),
    mel_energies_(opts.mel_opts.num_bins) {

  int32 num_bins = opts.mel_opts.num_bins;
  if (opts.num_ceps > num_bins)
    KALDI_ERR << "num-ceps cannot be larger than num-mel-bins."
              << " It should be smaller or equal. You provided num-ceps: "
              << opts.num_ceps << "  and num-mel-bins: "
              << num_bins;

  Matrix<BaseFloat> dct_matrix(num_bins, num_bins);
  ComputeDctMatrix(&dct_matrix);
  // Note that we include zeroth dct in either case.  If using the
  // energy we replace this with the energy.  This means a different
  // ordering of features than HTK.
  SubMatrix<BaseFloat> dct_rows(dct_matrix, 0, opts.num_ceps, 0, num_bins);
  dct_matrix_.Resize(opts.num_ceps, num_bins);
  dct_matrix_.CopyFromMat(dct_rows);  // subset of rows.
  if (opts.cepstral_lifter != 0.0) {
    lifter_coeffs_.Resize(opts.num_ceps);
    ComputeLifterCoeffs(opts.cepstral_lifter, &lifter_coeffs_);
  }
  if (opts.energy_floor > 0.0)
    log_energy_floor_ = Log(opts.energy_floor);

  int32 padded_window_size = opts.frame_opts.PaddedWindowSize();
  if ((padded_window_size & (padded_window_size-1)) == 0)  // Is a power of two...
    srfft_ = new SplitRadixRealFft<BaseFloat>(padded_window_size);

  // We'll definitely need the filterbanks info for VTLN warping factor 1.0.
  // [note: this call caches it.]
  GetMelBanks(1.0);
}

MfccComputer::MfccComputer(const MfccComputer &other):
    opts_(other.opts_), lifter_coeffs_(other.lifter_coeffs_),
    dct_matrix_(other.dct_matrix_),
    log_energy_floor_(other.log_energy_floor_),
    mel_banks_(other.mel_banks_),
    srfft_(NULL),
    mel_energies_(other.mel_energies_.Dim(), kUndefined) {
  for (std::map<BaseFloat, MelBanks*>::iterator iter = mel_banks_.begin();
       iter != mel_banks_.end(); ++iter)
    iter->second = new MelBanks(*(iter->second));
  if (other.srfft_ != NULL)
    srfft_ = new SplitRadixRealFft<BaseFloat>(*(other.srfft_));
}



MfccComputer::~MfccComputer() {
  for (std::map<BaseFloat, MelBanks*>::iterator iter = mel_banks_.begin();
      iter != mel_banks_.end();
      ++iter)
    delete iter->second;
  delete srfft_;
}

const MelBanks *MfccComputer::GetMelBanks(BaseFloat vtln_warp) {
  MelBanks *this_mel_banks = NULL;
  std::map<BaseFloat, MelBanks*>::iterator iter = mel_banks_.find(vtln_warp);
  if (iter == mel_banks_.end()) {
    this_mel_banks = new MelBanks(opts_.mel_opts,
                                  opts_.frame_opts,
                                  vtln_warp);
    mel_banks_[vtln_warp] = this_mel_banks;
  } else {
    this_mel_banks = iter->second;
  }
  return this_mel_banks;
}

}  // namespace kaldi
