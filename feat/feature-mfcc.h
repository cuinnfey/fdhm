// feat/feature-mfcc.h

// Copyright 2009-2011  Karel Vesely;  Petr Motlicek;  Saarland University
//           2014-2016  Johns Hopkins University (author: Daniel Povey)

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#ifndef KALDI_FEAT_FEATURE_MFCC_H_
#define KALDI_FEAT_FEATURE_MFCC_H_

#include <map>
#include <string>

#include "feat/feature-common.h"
#include "feat/feature-functions.h"
#include "feat/feature-window.h"
#include "feat/mel-computations.h"

namespace kaldi {
/// @addtogroup  feat FeatureExtraction
/// @{

//V/ HISTOGRAM MATCHING ADDITIONS /V//

struct vector_clump {
  VectorBase<BaseFloat> *cdf_1;
  VectorBase<BaseFloat> *vals_1;
  VectorBase<BaseFloat> *cdf_2;
  VectorBase<BaseFloat> *vals_2;
  VectorBase<BaseFloat> *cdf_3;
  VectorBase<BaseFloat> *vals_3;
};

/// Author: Cuinn Fey
/// Last Edited: 10/05/2020
/// Takes 3 vectors, source, template, and matched, computes the
/// histogram (or cdf) match (interpolation) between the source and
/// the template and stores it in the match. Returns the cdfs of
/// the source and the template as a pair of vectors.
vector_clump histMatch(
                 VectorBase<BaseFloat> *src,
                 VectorBase<BaseFloat> *tmp,
                 VectorBase<BaseFloat> *mtc,
                 BaseFloat silence_threshold);
/// Author: Cuinn Fey
/// Last Edited: 10/05/2020
/// Takes a vector of values. Finds and returns a matrix containing
/// the unique values, the indices they apear in, and how many times
/// they apear. Not very robust. Does not store more than one index
/// per unique value.
MatrixBase<BaseFloat>* unique_info(VectorBase<BaseFloat> *ar);

/// Author: Cuinn Fey
/// Last Edited: 10/05/2020
/// Takes an array of values and a vector of the same size and 
/// computes the cumulative sum of the vector and stores it in
/// the other vector.
void cumsum(VectorBase<BaseFloat> *cnts,
            VectorBase<BaseFloat> *quantiles);

/// Author: Cuinn Fey
/// Last Edited: 10/05/2020
/// Takes a target value, and two points. Computes the linear
/// interpolation and returns it as a float.
float interpolated(float target, float x1, float x2, float y1, float y2);

/// Author: Cuinn Fey
/// Last Edited: 10/05/2020
/// Takes two cdfs, the unique values of the second cdf, and an
/// vector the same size as the first vector(?). Computes the
/// linear interpolation between the two cdfs.
void linInterp(VectorBase<BaseFloat> *s_quan,
               VectorBase<BaseFloat> *s_vals,
               VectorBase<BaseFloat> *t_quan,
               VectorBase<BaseFloat> *t_vals,
               VectorBase<BaseFloat> *t_interp,
               BaseFloat silence_threshold);


//^/ HISTOGRAM MATCHING ADDITIONS /^//

/// MfccOptions contains basic options for computing MFCC features.
struct MfccOptions {
  FrameExtractionOptions frame_opts;
  MelBanksOptions mel_opts;
  int32 num_ceps;  // e.g. 13: num cepstral coeffs, counting zero.
  bool use_energy;  // use energy; else C0
  BaseFloat energy_floor;  // 0 by default; set to a value like 1.0 or 0.1 if
                           // you disable dithering.
  bool raw_energy;  // If true, compute energy before preemphasis and windowing
  BaseFloat cepstral_lifter;  // Scaling factor on cepstra for HTK compatibility.
                              // if 0.0, no liftering is done.
  bool htk_compat;  // if true, put energy/C0 last and introduce a factor of
                    // sqrt(2) on C0 to be the same as HTK.
  
  //V/ HISTOGRAM MATCHING ADDITIONS /V//
  bool hist_match;
  BaseFloat silence_threshold;
  bool plot_print;
  int omit_bin_first;
  int omit_bin_second;
  int omit_bin_third;

  //^/ HISTOGRAM MATCHING ADDITIONS /^//

  MfccOptions() : mel_opts(23),
                  // defaults the #mel-banks to 23 for the MFCC computations.
                  // this seems to be common for 16khz-sampled data,
                  // but for 8khz-sampled data, 15 may be better.
                  num_ceps(13),
                  use_energy(true),
                  energy_floor(0.0),
                  raw_energy(true),
                  cepstral_lifter(22.0),
                  //V/ HISTOGRAM MATCHING ADDITIONS /V//
                  hist_match(false),
                  silence_threshold(11.95),
                  plot_print(false),
                  omit_bin_first(21),
                  omit_bin_second(21),
                  omit_bin_third(21),
                  //^/ HISTOGRAM MATCHING ADDITIONS /^//
                  htk_compat(false) {}

  void Register(OptionsItf *opts) {
    frame_opts.Register(opts);
    mel_opts.Register(opts);
    opts->Register("num-ceps", &num_ceps,
                   "Number of cepstra in MFCC computation (including C0)");
    opts->Register("use-energy", &use_energy,
                   "Use energy (not C0) in MFCC computation");
    opts->Register("energy-floor", &energy_floor,
                   "Floor on energy (absolute, not relative) in MFCC computation. "
                   "Only makes a difference if --use-energy=true; only necessary if "
                   "--dither=0.0.  Suggested values: 0.1 or 1.0");
    opts->Register("raw-energy", &raw_energy,
                   "If true, compute energy before preemphasis and windowing");
    opts->Register("cepstral-lifter", &cepstral_lifter,
                   "Constant that controls scaling of MFCCs");
     
    //V/ HISTOGRAM MATCHING ADDITIONS /V//
    opts->Register("hist-match", &hist_match,
                   "If true, use histogram matching to match source to template");
    opts->Register("silence-threshold", &silence_threshold,
                   "Sets lower bound on histogram matching in dB");
    opts->Register("plot-print", &plot_print,
                   "Determines whether cdf and spectrogram data is printed to file");    
    opts->Register("omit-bin-first", &omit_bin_first,
                   "Sets first bin to omit in histmatching");
    opts->Register("omit-bin-second", &omit_bin_second,
                   "Sets second bin to omit in histmatching");
    opts->Register("omit-bin-third", &omit_bin_third,
                   "Sets third bin to omit in histmatching");
     
    //^/ HISTOGRAM MATCHING ADDITIONS /^//


    opts->Register("htk-compat", &htk_compat,
                   "If true, put energy or C0 last and use a factor of sqrt(2) on "
                   "C0.  Warning: not sufficient to get HTK compatible features "
                   "(need to change other parameters).");
  }
};



// This is the new-style interface to the MFCC computation.
class MfccComputer {
 public:
  typedef MfccOptions Options;
  explicit MfccComputer(const MfccOptions &opts);
  MfccComputer(const MfccComputer &other);

  const FrameExtractionOptions &GetFrameOptions() const {
    return opts_.frame_opts;
  }

  int32 Dim() const { return opts_.num_ceps; }

  bool NeedRawLogEnergy() const { return opts_.use_energy && opts_.raw_energy; }

  /**
     Function that computes one frame of features from
     one frame of signal.

     @param [in] signal_raw_log_energy The log-energy of the frame of the signal
         prior to windowing and pre-emphasis, or
         log(numeric_limits<float>::min()), whichever is greater.  Must be
         ignored by this function if this class returns false from
         this->NeedsRawLogEnergy().
     @param [in] vtln_warp  The VTLN warping factor that the user wants
         to be applied when computing features for this utterance.  Will
         normally be 1.0, meaning no warping is to be done.  The value will
         be ignored for feature types that don't support VLTN, such as
         spectrogram features.
     @param [in] signal_frame  One frame of the signal,
       as extracted using the function ExtractWindow() using the options
       returned by this->GetFrameOptions().  The function will use the
       vector as a workspace, which is why it's a non-const pointer.
     @param [out] feature  Pointer to a vector of size this->Dim(), to which
         the computed feature will be written.
  */
  void Compute(BaseFloat signal_raw_log_energy,
               BaseFloat vtln_warp,
               VectorBase<BaseFloat> *signal_frame,
               VectorBase<BaseFloat> *feature);

   //V/ HISTOGRAM MATCHING ADDITIONS /V//
   
  void ComputeDCT(BaseFloat signal_raw_log_energy,
               BaseFloat vtln_warp,
               VectorBase<BaseFloat> *signal_frame,
               VectorBase<BaseFloat> *feature,
               VectorBase<BaseFloat> *feature_temp);
   

   //^/ HISTOGRAM MATCHING ADDITIONS /^//

  ~MfccComputer();
 private:
  // disallow assignment.
  MfccComputer &operator = (const MfccComputer &in);

 protected:
  const MelBanks *GetMelBanks(BaseFloat vtln_warp);

  MfccOptions opts_;
  Vector<BaseFloat> lifter_coeffs_;
  Matrix<BaseFloat> dct_matrix_;  // matrix we left-multiply by to perform DCT.
  BaseFloat log_energy_floor_;
  std::map<BaseFloat, MelBanks*> mel_banks_;  // BaseFloat is VTLN coefficient.
  SplitRadixRealFft<BaseFloat> *srfft_;

  // note: mel_energies_ is specific to the frame we're processing, it's
  // just a temporary workspace.
  Vector<BaseFloat> mel_energies_;
};

typedef OfflineFeatureTpl<MfccComputer> Mfcc;


/// @} End of "addtogroup feat"
}  // namespace kaldi


#endif  // KALDI_FEAT_FEATURE_MFCC_H_
