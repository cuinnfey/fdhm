// featbin/compute-mfcc-feats.cc

// Copyright 2009-2012  Microsoft Corporation
//                      Johns Hopkins University (author: Daniel Povey)

// See ../../COPYING for clarification regarding multiple authors
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//  http://www.apache.org/licenses/LICENSE-2.0
//
// THIS CODE IS PROVIDED *AS IS* BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
// KIND, EITHER EXPRESS OR IMPLIED, INCLUDING WITHOUT LIMITATION ANY IMPLIED
// WARRANTIES OR CONDITIONS OF TITLE, FITNESS FOR A PARTICULAR PURPOSE,
// MERCHANTABLITY OR NON-INFRINGEMENT.
// See the Apache 2 License for the specific language governing permissions and
// limitations under the License.

#include "matrix/kaldi-vector.h"
#include "base/kaldi-common.h"
#include "feat/feature-mfcc.h"
#include "feat/wave-reader.h"
#include "util/common-utils.h"
#include <fstream>

int main(int argc, char *argv[]) {
  try {
    using namespace kaldi;
    const char *usage =
        "Create MFCC feature files.\n"
        "Usage:  compute-mfcc-feats [options...] <wav-rspecifier> "
        "<feats-wspecifier>\n";

    // Construct all the global objects.
    ParseOptions po(usage);
    MfccOptions mfcc_opts;
    // Define defaults for global options.
    bool subtract_mean = false;
    BaseFloat vtln_warp = 1.0;
    std::string vtln_map_rspecifier;
    std::string utt2spk_rspecifier;
    int32 channel = -1;
    BaseFloat min_duration = 0.0;
    std::string output_format = "kaldi";
    std::string utt2dur_wspecifier;

    // Register the MFCC option struct.
    mfcc_opts.Register(&po);

    // Register the options.
    po.Register("output-format", &output_format, "Format of the output "
                "files [kaldi, htk]");
    po.Register("subtract-mean", &subtract_mean, "Subtract mean of each "
                "feature file [CMS]; not recommended to do it this way. ");
    po.Register("vtln-warp", &vtln_warp, "Vtln warp factor (only applicable "
                "if vtln-map not specified)");
    po.Register("vtln-map", &vtln_map_rspecifier, "Map from utterance or "
                "speaker-id to vtln warp factor (rspecifier)");
    po.Register("utt2spk", &utt2spk_rspecifier, "Utterance to speaker-id map "
                "rspecifier (if doing VTLN and you have warps per speaker)");
    po.Register("channel", &channel, "Channel to extract (-1 -> expect mono, "
                "0 -> left, 1 -> right)");
    po.Register("min-duration", &min_duration, "Minimum duration of segments "
                "to process (in seconds).");
    po.Register("write-utt2dur", &utt2dur_wspecifier, "Wspecifier to write "
                "duration of each utterance in seconds, e.g. 'ark,t:utt2dur'.");

    po.Read(argc, argv);

    if (po.NumArgs() != 2) {
      po.PrintUsage();
      exit(1);
    }

    std::string wav_rspecifier = po.GetArg(1);

    std::string output_wspecifier = po.GetArg(2);

    Mfcc mfcc(mfcc_opts);

    if (utt2spk_rspecifier != "" && vtln_map_rspecifier == "")
      KALDI_ERR << ("The --utt2spk option is only needed if "
                    "the --vtln-map option is used.");
    RandomAccessBaseFloatReaderMapped vtln_map_reader(vtln_map_rspecifier,
                                                      utt2spk_rspecifier);

    SequentialTableReader<WaveHolder> reader(wav_rspecifier);

    //V/ HISTOGRAM MATCHING ADDITIONS /V//
    // if this line throws an error, then create the file and folders
    // data/ldc_snapshot/wav.cp to appease this line
    SequentialTableReader<WaveHolder> reader_ldc("scp,p:data/ldc_snapshot/wav.scp");
    //^/ HISTOGRAM MATCHING ADDITIONS /^//


    BaseFloatMatrixWriter kaldi_writer;  // typedef to TableWriter<something>.
    TableWriter<HtkMatrixHolder> htk_writer;

    if (output_format == "kaldi") {
      if (!kaldi_writer.Open(output_wspecifier))
        KALDI_ERR << "Could not initialize output with wspecifier "
                  << output_wspecifier;
    } else if (output_format == "htk") {
      if (!htk_writer.Open(output_wspecifier))
        KALDI_ERR << "Could not initialize output with wspecifier "
                  << output_wspecifier;
    } else {
      KALDI_ERR << "Invalid output_format string " << output_format;
    }

    DoubleWriter utt2dur_writer(utt2dur_wspecifier);


    //V/ HISTOGRAM MATCHING ADDITIONS /V//
    std::string recording = "ldc_snaphot.wav";

    const WaveData &wave_ldc = reader_ldc.Value();
    BaseFloat samp_freq_ldc = wave_ldc.SampFreq();
    //^/ HISTOGRAM MATCHING ADDITIONS /^//

    int32 num_utts = 0, num_success = 0;
    for (; !reader.Done(); reader.Next()) {
      num_utts++;
      std::string utt = reader.Key();
      const WaveData &wave_data = reader.Value();
      if (wave_data.Duration() < min_duration) {
        KALDI_WARN << "File: " << utt << " is too short ("
                   << wave_data.Duration() << " sec): producing no output.";
        continue;
      }
      int32 num_chan = wave_data.Data().NumRows(), this_chan = channel;
      {  // This block works out the channel (0=left, 1=right...)
        KALDI_ASSERT(num_chan > 0);  // should have been caught in
        // reading code if no channels.
        if (channel == -1) {
          this_chan = 0;
          if (num_chan != 1)
            KALDI_WARN << "Channel not specified but you have data with "
                       << num_chan  << " channels; defaulting to zero";
        } else {
          if (this_chan >= num_chan) {
            KALDI_WARN << "File with id " << utt << " has "
                       << num_chan << " channels but you specified channel "
                       << channel << ", producing no output.";
            continue;
          }
        }
      }
      BaseFloat vtln_warp_local;  // Work out VTLN warp factor.
      if (vtln_map_rspecifier != "") {
        if (!vtln_map_reader.HasKey(utt)) {
          KALDI_WARN << "No vtln-map entry for utterance-id (or speaker-id) "
                     << utt;
          continue;
        }
        vtln_warp_local = vtln_map_reader.Value(utt);
      } else {
        vtln_warp_local = vtln_warp;
      }

      SubVector<BaseFloat> waveform(wave_data.Data(), this_chan);
      //V/ HISTOGRAM MATCHING ADDITIONS /V//
      SubVector<BaseFloat> waveform_ldc(wave_ldc.Data(), 0);      
      Matrix<BaseFloat> features_ldc;
      bool hist_match = mfcc_opts.hist_match;
      //^/ HISTOGRAM MATCHING ADDITIONS /^//

      Matrix<BaseFloat> features;
      try {
        mfcc.ComputeFeatures(waveform, wave_data.SampFreq(),
                             vtln_warp_local, &features);
        //V/ HISTOGRAM MATCHING ADDITIONS /V//
        /// *Note* for the line above, the DCT step was removed
        /// from the mfcc.ComputeFeatures process but
        /// is added back in at the end of the following process:         
 
        /// Initialize another features mat
        Matrix<BaseFloat> features_temp(features.NumRows(), features.NumCols());
        
        /// Compute DCT-less mfcc features for ldc_snapshot
        mfcc.ComputeFeatures(waveform_ldc, wave_ldc.SampFreq(),
                             vtln_warp_local, &features_ldc);

        /// Initialize the Base versions of temp and ldc features so we can use
        /// the functions VectorBase and MatrixBase provide over standard 
        /// Matrix and Vector classes
        MatrixBase<BaseFloat> *features_temp_base = new Matrix<BaseFloat>(features);
        Matrix<BaseFloat> features_temp_mat = Matrix<BaseFloat>(*features_temp_base, kNoTrans);      
                  KALDI_VLOG(2) << "features_temp_base = " << *features_temp_base;     
        if (hist_match) {
          MatrixBase<BaseFloat> *features_ldc_base = new Matrix<BaseFloat>(features_ldc); 
         
          /// Make a submatrix of features_ldc that is the same size as
          /// features temp
          MatrixBase<BaseFloat> *features_ldc_base_small = new SubMatrix<BaseFloat>(
                                                              features_ldc_base->Data(),
                                                              features_temp_base->NumRows(),
                                                              features_temp_base->NumCols(),
                                                              features_temp_base->Stride());

          /// Initialized output matrix of histMatch process
          MatrixBase<BaseFloat> *matched = new Matrix<BaseFloat>(features_temp);
          matched->CopyFromMat(*features_temp_base);

          /// Intialize vectors to store a frequency frame at a time to
          /// be histogram matched
          VectorBase<BaseFloat> *bin_src = new Vector<BaseFloat>(
                                                            features_temp_base->NumRows());
          VectorBase<BaseFloat> *bin_tmp = new Vector<BaseFloat>(
                                                       features_ldc_base_small->NumRows());
          VectorBase<BaseFloat> *bin_mtc = new Vector<BaseFloat>(
                                                            features_temp_base->NumRows());

          /// Intialize Matrices to hold cdf values of source and template
          MatrixBase<BaseFloat> *cdfs_src = new Matrix<BaseFloat>(
                                                              features_temp_base->NumRows(),
                                                              features_temp_base->NumCols());
          MatrixBase<BaseFloat> *cdfs_tmp = new Matrix<BaseFloat>(
                                                              features_temp_base->NumRows(),
                                                              features_temp_base->NumCols());
          MatrixBase<BaseFloat> *cdfs_mtc = new Matrix<BaseFloat>(
                                                              features_temp_base->NumRows(),
                                                              features_temp_base->NumCols());
          MatrixBase<BaseFloat> *vals_src = new Matrix<BaseFloat>(
                                                              features_temp_base->NumRows(),
                                                              features_temp_base->NumCols());
          MatrixBase<BaseFloat> *vals_tmp = new Matrix<BaseFloat>(
                                                              features_temp_base->NumRows(),
                                                              features_temp_base->NumCols());
          MatrixBase<BaseFloat> *vals_mtc = new Matrix<BaseFloat>(
                                                              features_temp_base->NumRows(),
                                                              features_temp_base->NumCols());

          // "omit" logic and variables are depricated functionality and can be ignored/removed
          int omit_bin_first = mfcc_opts.omit_bin_first; 
          int omit_bin_second = mfcc_opts.omit_bin_second;
          int omit_bin_third = mfcc_opts.omit_bin_third;  
          /// do histogram matching 
          for (int i = 0; i < features_temp_base->NumCols(); i++) {
            if (i != omit_bin_first || i != omit_bin_second || i != omit_bin_third) {
              bin_src->CopyColFromMat(*features_temp_base, i);
              bin_tmp->CopyColFromMat(*features_ldc_base_small, i);
              bin_mtc->CopyColFromMat(*matched, i);

              struct vector_clump cdfs = histMatch(bin_src, bin_tmp, bin_mtc, mfcc_opts.silence_threshold);
          
              cdfs_src->CopyColFromVec(*cdfs.cdf_1, i);
              vals_src->CopyColFromVec(*cdfs.vals_1, i);
              cdfs_tmp->CopyColFromVec(*cdfs.cdf_2, i);
              vals_tmp->CopyColFromVec(*cdfs.vals_2, i);
              cdfs_mtc->CopyColFromVec(*cdfs.cdf_3, i); 
              vals_mtc->CopyColFromVec(*cdfs.vals_3, i);         

              matched->CopyColFromVec(*bin_mtc, i);
           
            } else {
              bin_src->CopyColFromMat(*features_temp_base, i);
              bin_tmp->CopyColFromMat(*features_ldc_base_small, i);
              bin_mtc->CopyColFromMat(*matched, i);

              struct vector_clump cdfs = histMatch(bin_src, bin_tmp, bin_mtc, mfcc_opts.silence_threshold);
          
              cdfs_src->CopyColFromVec(*cdfs.cdf_1, i);
              vals_src->CopyColFromVec(*cdfs.vals_1, i);
              cdfs_tmp->CopyColFromVec(*cdfs.cdf_2, i);
              vals_tmp->CopyColFromVec(*cdfs.vals_2, i);
              cdfs_mtc->CopyColFromVec(*cdfs.cdf_1, i); 
              vals_mtc->CopyColFromVec(*cdfs.vals_1, i);         

              matched->CopyColFromVec(*bin_src, i);
            }
          }
          
          // Optional printing of log Mel-Spectrograms to csv files
          // Need to modify path in which to print the csv files to
          // A python script is supplied to plot these csv files
          if (mfcc_opts.plot_print) {      
            std::ofstream outFile0("path/to/kaldi/egs/project/data/ldc_snapshot/vals_src.txt",
                                   std::ios_base::app);
            for (int i = 0; i < vals_src->NumCols(); i++) {
              for (int j = 0; j < vals_src->NumRows(); j++) {
                outFile0 << *(vals_src->Data() + j * vals_src->Stride() + i) << " ";
              }
              outFile0 << "\n";
            }
            outFile0 << ",";

      
            std::ofstream outFile1("path/to/kaldi/egs/project/data/ldc_snapshot/vals_tmp.txt",
                                   std::ios_base::app);
            for (int i = 0; i < vals_tmp->NumCols(); i++) {
              for (int j = 0; j < vals_tmp->NumRows(); j++) {
                outFile1 << *(vals_tmp->Data() + j * vals_tmp->Stride() + i) << " ";
              }
              outFile1 << "\n";
            }
            outFile1 << ",";

            std::ofstream outFile2("path/to/kaldi/egs/project/data/ldc_snapshot/vals_mtc.txt",
                                   std::ios_base::app);
            for (int i = 0; i < vals_mtc->NumCols(); i++) {
              for (int j = 0; j < vals_mtc->NumRows(); j++) {
                outFile2 << *(vals_mtc->Data() + j * vals_mtc->Stride() + i) << " ";
              }
              outFile2 << "\n";
            }
            outFile2 << ",";

 
      
            std::ofstream outFile3("path/to/kaldi/egs/project/data/ldc_snapshot/cdfs_src.txt",
                                   std::ios_base::app);
            for (int i = 0; i < cdfs_src->NumCols(); i++) {
              for (int j = 0; j < cdfs_src->NumRows(); j++) {
                outFile3 << *(cdfs_src->Data() + j * cdfs_src->Stride() + i) << " ";
              }
              outFile3 << "\n";
            }
            outFile3 << ",";
      
            std::ofstream outFile4("path/to/kaldi/egs/project/data/ldc_snapshot/cdfs_tmp.txt",
                                   std::ios_base::app);
            for (int i = 0; i < cdfs_tmp->NumCols(); i++) {
              for (int j = 0; j < cdfs_tmp->NumRows(); j++) {
                outFile4 << *(cdfs_tmp->Data() + j * cdfs_tmp->Stride() + i) << " ";
              } 
              outFile4 << "\n";
            }
            outFile4 << ",";

            std::ofstream outFile5("path/to/kaldi/egs/project/data/ldc_snapshot/cdfs_mtc.txt",
                                   std::ios_base::app);
            for (int i = 0; i < cdfs_mtc->NumCols(); i++) {
              for (int j = 0; j < cdfs_mtc->NumRows(); j++) {
                outFile5 << *(cdfs_mtc->Data() + j * cdfs_mtc->Stride() + i) << " ";
              }
              outFile5 << "\n";
            }
            outFile5 << ",";

      
            std::ofstream outFile6("path/to/kaldi/egs/project/data/ldc_snapshot/log_mel_specs_src.txt",
                                   std::ios_base::app);
            for (int i = 0; i < features_temp_base->NumCols(); i++) {
              for (int j = 0; j < features_temp_base->NumRows(); j++) {
                outFile6 << *(features_temp_base->Data() + j * features_temp_base->Stride() 
                                                                                  + i) << " ";
              }
              outFile6 << "\n";
            }
            outFile6 << ",";


            std::ofstream outFile7("path/to/kaldi/egs/project/data/ldc_snapshot/log_mel_specs_tmp.txt",
                                   std::ios_base::app);
            for (int i = 0; i < features_ldc_base_small->NumCols(); i++) {
              for (int j = 0; j < features_ldc_base_small->NumRows(); j++) {
                outFile7 << *(features_ldc_base_small->Data() + j * features_ldc_base_small->Stride() 
                                                                                 + i) << " ";
              }
              outFile7 << "\n";
            }
            outFile7 << ",";


            std::ofstream outFile8("path/to/kaldi/egs/project/data/ldc_snapshot/log_mel_specs_mtc.txt",
                                   std::ios_base::app);
            for (int i = 0; i < matched->NumCols(); i++) {
              for (int j = 0; j < matched->NumRows(); j++) {
                outFile8 << *(matched->Data() + j * matched->Stride() + i) << " ";
              }
              outFile8 << "\n";
            }
            outFile8 << ",";
          } 
         
          Matrix<BaseFloat> matched_no_base = Matrix<BaseFloat>(*matched, kNoTrans);      
          /// Compute DCT so that the rest of the process works as before       
          mfcc.ComputeFeaturesDCT(waveform, wave_data.SampFreq(),
                                  vtln_warp_local, &matched_no_base, &features);
              
        } else { 
          mfcc.ComputeFeaturesDCT(waveform, wave_data.SampFreq(),
                                  vtln_warp_local, &features_temp_mat, &features);   
        }
        //^/ HISTOGRAM MATCHING ADDITIONS /^//
      } catch (...) {
        KALDI_WARN << "Failed to compute features for utterance " << utt;
        continue;
      }
      if (subtract_mean) {
        Vector<BaseFloat> mean(features.NumCols());
        mean.AddRowSumMat(1.0, features);
        mean.Scale(1.0 / features.NumRows());
        for (int32 i = 0; i < features.NumRows(); i++)
          features.Row(i).AddVec(-1.0, mean);
      }
      if (output_format == "kaldi") {
        kaldi_writer.Write(utt, features);
      } else {
        std::pair<Matrix<BaseFloat>, HtkHeader> p;
        p.first.Resize(features.NumRows(), features.NumCols());
        p.first.CopyFromMat(features);
        HtkHeader header = {
          features.NumRows(),
          100000,  // 10ms shift
          static_cast<int16>(sizeof(float)*(features.NumCols())),
          static_cast<uint16>( 006 | // MFCC
          (mfcc_opts.use_energy ? 0100 : 020000)) // energy; otherwise c0
        };
        p.second = header;
        htk_writer.Write(utt, p);
      }
      if (utt2dur_writer.IsOpen()) {
        utt2dur_writer.Write(utt, wave_data.Duration());
      }
      if (num_utts % 10 == 0)
        KALDI_LOG << "Processed " << num_utts << " utterances";
      KALDI_VLOG(2) << "Processed features for key " << utt;
      num_success++;
    }
    KALDI_LOG << " Done " << num_success << " out of " << num_utts
              << " utterances.";
    return (num_success != 0 ? 0 : 1);
  } catch(const std::exception &e) {
    std::cerr << e.what();
    return -1;
  }
}
